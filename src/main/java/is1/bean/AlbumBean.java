package is1.bean;

import is1.model.Album;

public class AlbumBean {
	private Long id;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Album createAlbum() {
		Album album = new Album();
		album.setId(id);
		album.setName(name);
		return album;
	}
}
