package is1.repository.jpa;

import java.util.List;

import is1.model.Album;
import is1.repository.AlbumRepository;

import org.springframework.stereotype.Repository;

@Repository
public class JpaAlbumRepository extends JpaBaseRepository<Album, Long> implements AlbumRepository {

	@Override
	public List<Album> topTenLastWeek() {
		// ORDER BY rank DESC LIMIT 10
		return null;
	}

}
