package is1.repository;

import is1.model.Album;

import java.util.List;

public interface AlbumRepository extends BaseRepository<Album, Long> {
	List<Album> topTenLastWeek();
}
