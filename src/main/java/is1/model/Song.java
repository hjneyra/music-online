package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "song")
public class Song implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "SONG_ID_GENERATOR", sequenceName = "SONG_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SONG_ID_GENERATOR")
	private Long id;

	@Column
	private String name;
	
	@ManyToOne
	private Album album;

	@ManyToMany
	private List<Playlist> playlists;

	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public List<Playlist> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(List<Playlist> playlists) {
		this.playlists = playlists;
	}
}
