package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "playlist")
public class Playlist implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "PLAYLIST_ID_GENERATOR", sequenceName = "PLAYLIST_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYLIST_ID_GENERATOR")
	private Long id;

	@Column
	private String name;

	@ManyToMany(mappedBy="playlists")
	@JoinTable(name="playlist_song",
    joinColumns=
        @JoinColumn(name="playlist_id", referencedColumnName="id"),
    inverseJoinColumns=
        @JoinColumn(name="song_id", referencedColumnName="id")
    )
	private List<Song> songs;

	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
}
