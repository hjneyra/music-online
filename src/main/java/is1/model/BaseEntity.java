package is1.model;

public interface BaseEntity<PK> {
	PK getId();
}