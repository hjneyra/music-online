package is1.controller;

import is1.bean.AlbumBean;
import is1.bean.SignupBean;
import is1.model.Album;
import is1.repository.AlbumRepository;
import is1.support.web.MessageHelper;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "album")
public class AlbumController {
	@Inject
	AlbumRepository albumRepository;

	@RequestMapping(value = "edit")
	public String signup(Model model) {
		model.addAttribute(new SignupBean());
		return "album/edit";
	}

	@RequestMapping(value = "edit", method = RequestMethod.POST)
	public String signup(@Valid @ModelAttribute AlbumBean albumBean,
			Errors errors, RedirectAttributes ra) {
		Album album = albumRepository.save(albumBean.createAlbum());
		MessageHelper.addSuccessAttribute(ra, "album.create.success");
		return "redirect:/album/" + album.getId();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public Album findById(@PathVariable("id") Long albumId) {
		Assert.notNull(albumId);
		return albumRepository.findById(albumId);
	}
}
